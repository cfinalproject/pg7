#include <vector>
#include <time.h>
#include <stdlib.h>
#include <string>
#include <iostream>

#include "AI.h"
#include "Player.h"

using std::string;
using std::cout;
using std::endl;

AI::AI()
{
    type = 2;
    moh = 0;
}

int AI::makemove(){
    //srand(time(NULL));
    if (phand.size() == 0){
        int num1 = 2;
        return num1;
    }
    int num1 = rand() % 4;
    return num1 + 1;
}

string AI::decision(){
    ///exception handling: AI decision. array of goods types.
    ///Generate random number and if it fails and tries
    ///again, generate a new random number
    //srand(time(NULL));
    if (moh == 0) {
        int num = rand() % 5;
        moh = 1;
        return market[num];
    }
    else {
        int sizee = phand.size();
        if(sizee == 0) {
            sizee = 1;
        }
        int num = rand() % sizee;
        moh = 0;
        return phand[num].getType();
    }
//    return "Cloth";
}

int AI::intdecision(){
    //srand(time(NULL));
    unsigned int num1 = rand() % 5;
    if (num1 > phand.size()) {
        num1 = rand() % phand.size();
    }
    return num1;
}
