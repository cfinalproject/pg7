#include <iostream>
#include <string>
#include "Game.h"

using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::cerr;

int main(){

    int choice;
    string name1, name2;

    cout << "Welcome to Jaipur! Choose your player types:" << endl;
    cout << "(1) Human/Human" << endl;
    cout << "(2) Human/Computer" << endl;
    cout << "(3) Computer/Computer" << endl;
    cin >> choice;

    switch(choice){
    case(1):
    {
        cout << "Enter Player 1 name:" << endl;
        cin >> name1;
        cout << "Enter Player 2 name:" << endl;
        cin >> name2;
        Game g1(name1, name2);
        g1.strut();
        cout << "Playing round 1!" << endl;
        g1.playround();
        cout << "Playing round 2!" << endl;
        g1.playround();
        break;
    }

    case(2):
    {
        cout << "Enter Player 1 name:" << endl;
        cin >> name1;
        Game g2(name1);
        cout << "Playing round 1!" << endl;
        g2.playround();
        cout << "Playing round 2!" << endl;
        g2.playround();
        break;
    }

    case(3):
    {
        Game g3;
        cout << "Playing round 1!" << endl;
        g3.playround();
        cout << "Playing round 2!" << endl;
        g3.playround();
        break;
    }
}
}
