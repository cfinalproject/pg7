#include <iostream>
#include <string>
#include "Card.h"
#include "Token.h"
#include "Player.h"
#include <map>
//#include "Move.h"
//#include "Game.h"

using std::string;
using std::map;
using std::vector;
using std::cout;
using std::endl;

Player::Player()
{
//    name = "blah";
//    herd.push_back();
//    phand.push_back();
    points = 0;
}

Player::~Player()
{

}

void Player::addcard(const Card& c)
{

    if(c.getRank() == 9001)
    {
	herd.push_back(c);
        //add to vector herd
    }
    else
    {
	phand.push_back(c);
        //add to hand
    }
}
void Player::remove_card(Card& card)
{
    if (card.getRank() == 9001)
    {
	herd.erase(herd.begin());
    }
    else
    {
	unsigned int i;
	for (i = 0; i < phand.size(); i++) {
	    if (phand[i] == card) {
		break;
	    }
	}

	phand.erase(phand.begin() + i);
    }
}

void Player::printHand()
{
    cout << "Player: " << name << endl;

    vector<Card>::const_iterator i; //read-only iterator
    int num, num2;
    num = num2 = 1;

//    cout << phand.size() << endl;

    if (phand.size() == 0){
        cout << "No goods" << endl;
    }
    else {
	cout << "Hand" << endl;
        for(i = phand.begin(); i != phand.end(); i++){
            cout << num << ") " << i->getType() << endl;
            num++;
        }
	cout << endl;
    }
    if (herd.size() == 0){
        cout << "No camels" << endl;
    }
    else {
	cout << "Herd" << endl;
        for(i = herd.begin(); i != herd.end(); i++){
            cout << num2 << ") " << i->getType() << endl;
            num2++;
        }
	cout << endl;
    }
}
