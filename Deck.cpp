#include <vector>
#include <stdlib.h>
#include <time.h>
#include "Card.h"
#include "Deck.h"

#define DERP 9001

using std::string;

/*
 Description: Deck contains all of
 the cards in the game. It contains
 functions for shuffling the deck and
 distributing cards to the players
 and the market.
*/

Deck::Deck() {
	int i, j;
	string c_type[] = {"Diamonds", "Gold", "Silver", "Cloth", "Spice", "Leather", "Camel"};
	int c_rank[] = {2, 2, 2, 1, 1, 1, DERP};
	int d_num[] = {6, 6, 6, 8, 8, 10, 11};

	for (i = 0; i < 7; i++) {
		for (j = 0; j < d_num[i]; j++) {
			Card gcard(c_type[i], c_rank[i]);
			cards.push_back(gcard);
		}
    }
}

void Deck::shuffle()
{
	int i, num, temp_r;
	string temp_t;

	srand(time(NULL));
	for (i = 0; i < 52; i++) {
		num = rand() % 52;
		temp_r = cards[i].rank;
		temp_t = cards[i].type;
		cards[i].rank = cards[num].rank;
		cards[i].type = cards[num].type;
		cards[num].rank = temp_r;
		cards[num].type = temp_t;
	}
}
