#ifndef TOKEN_H_INCLUDED
#define TOKEN_H_INCLUDED
#include <string>
#include <map>
#include <vector>
//#include "Player.h"
//#include "Market.h"

/**
 Description: Token is an abstract base class.
 Concrete derived classes include Tgood, Tcamel,
 and Bonus. A Token represents the common aspects
 of all tokens, including a type.
*/
class Token
{
    friend class Player;
    friend class Market;
    friend class Move;

public:
    Token();
    Token(std::string, int);
    std::map<std::string, std::vector<int> > token;
    //int value;
    //std::string type;

///    getType() { return type; };
///    getValue() { return value; };
};

#endif // TOKEN_H_INCLUDED
