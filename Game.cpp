#include <iostream>
#include <algorithm>
#include <map>
#include <sstream>
#include <string>
#include "Player.h"
#include "Market.h"
#include "Game.h"
#include "Bonus.h"
#include "Tgood.h"
#include "AI.h"
#include "Human.h"

#define FULL_HAND 7

using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::vector;
using std::map;

Game::Game()
{
    players.push_back(new AI()); //AI
    players.push_back(new AI()); //AI
    players[0]->name = "Player 1";
    players[0]->isWinner = true;
    players[1]->name = "Player 2";
    players[1]->isWinner = false;
}

Game::Game(string name1)
{
    players.push_back(new Human()); //human
    players.push_back(new AI()); //AI
    players[0]->name = name1;
    players[0]->isWinner = true;
    players[1]->name = "Player 2";
    players[1]->isWinner = false;
}

Game::Game(string name1, string name2)
{
    players.push_back(new Human()); //human
    players.push_back(new Human()); //human
    players[0]->name = name1;
    players[0]->isWinner = true;
    players[1]->name = name2;
    players[1]->isWinner = false;
}

void Game::playround()
{
    strut();
    int i = 0;
    int tokencount = 0;
    deal();
    while(market.deck.cards.size() != 0 && tokencount != 3 &&(players[0]->seal != 2 || players[1]->seal !=2))   //i am changed
    {

    if (i == 0)
    {
        cout << "Printing market:" << endl;
        printMarket(market.deck.cards.size());
        players[0]->printHand();
        cout << endl << "Points: " << players[0]->points << endl;
        cout << endl << players[0]->getName() <<"'s turn:" << endl;
        mm:
        try{        ///exception handling
            makemove(*players[0]);
        }
        catch(char const* mes){
            cout << mes << endl;
            goto mm;
        }
        cout << players[0]->getName() << "'s new hand:" << endl;
        players[0]->printHand();
        i++;
    }

    else
    {
        cout << "Printing market:" << endl;
        printMarket(market.deck.cards.size());
        players[1]->printHand();
        cout << "Points" << endl << players[1]->points << endl;
        cout << players[1]->getName() << "'s turn:" << endl;
        mm2:
        try{
            makemove(*players[1]);
        }
        catch(char const* mes){
            cout<<mes<<endl;
            goto mm2;
        }
        cout << players[1]->getName() << "'s new hand:" << endl;
        players[1]->printHand();
        i = 0;
    }
    }
//========= End of while loop ==============================
    if(players[0]->getHerd() > players[1]->getHerd()){
        players[0]->points = players[0]->points + 5;
        cout<<players[0]->name<<" got the camel token this round!\n"<<endl<<endl;
    }
    else {
        players[1]->points = players[1]->points + 5;
        cout<<players[1]->name<<" got the camel token this round!"<<endl<<endl;
    }

    if(players[0]->points > players[1]->points) {
        cout<<"The winner of this round is "<<players[0]->name<<endl<<endl;
        players[0]->seal++;
        players[1]->isWinner = false;
    }

    else {
        cout<<"The winner of this round is "<<players[1]->name<<endl<<endl;
        players[1]->seal++;
        players[1]->isWinner = true;
    }

//==== Save all the vectors that need to be put back into the deck ======
    vector<Card> nDeck = market.deck.cards;
    vector<Card> p1hand = players[0]->phand;
    vector<Card> p1herd = players[0]->herd;
    vector<Card> p2hand = players[1]->phand;
    vector<Card> p2herd = players[1]->herd;
    vector<Card> mkthand = market.mhand;
    vector<Card> dscd = market.discard;

//=== Insert them all into the new deck ======
    nDeck.insert(nDeck.end(), p1hand.begin(), p1hand.end());
    nDeck.insert(nDeck.end(), p1herd.begin(), p1herd.end());
    nDeck.insert(nDeck.end(), p2hand.begin(), p2hand.end());
    nDeck.insert(nDeck.end(), p2herd.begin(), p2herd.end());
    nDeck.insert(nDeck.end(), mkthand.begin(), mkthand.end());
    nDeck.insert(nDeck.end(), dscd.begin(), dscd.end());

//=== Clear the vectors after they have been copied to the new deck ====
    players[0]->phand.clear();
	players[0]->herd.clear();
	players[1]->phand.clear();
	players[1]->herd.clear();
	market.mhand.clear();
	market.discard.clear();

//=== Insert the new deck into the market's deck ====
    market.deck.cards.insert(market.deck.cards.end(), nDeck.begin(), nDeck.end());

//=== Declare the winner ====
    if(players[0]->seal==2)
    {
        cout << "Congratulations " << players[0]->name << "! You are the best salesman in Jaipur! May you reign for eons to come!!!" << endl;
    }

    else if(players[1]->seal==2)
    {
        cout << "Congratulations " << players[1]->name << "! You are the best salesman in Jaipur! May you reign for eons to come!!!" << endl;
    }
    strut();
}


void Game::deal()
{
    int i, j;
    market.deck.shuffle();

    for (i = 0; i < 5; i++) {
        Card c_temp;
        c_temp = market.deck.cards.back();
        market.add_card(c_temp);
        market.deck.cards.pop_back();
    }

    for(int i = 0; i < 2; i++)
    {
        for (j = 0; j < 5; j++)
        {
            Card c_temp;
            c_temp = market.deck.cards.back();
            players[i]->addcard(c_temp);
            market.deck.cards.pop_back();
        }
    }
}

void Game::makemove(Player& player)
{
    player.market.clear();
    for (int i = 0; i < 5; i++)
    {
        player.market.push_back(market.mhand[i].getType());
    }

    int num = player.makemove();

    switch(num){
        case(1):    ///take camels
        {
            ///exception handling
            vector<Card>::iterator iterate;
            iterate = market.mhand.begin();
            bool found = false;
            while(iterate != market.mhand.end())
            {
                if(iterate->getType() == "Camel")
                {
                    found = true;
                }
                iterate++;
            }
            if(found == false)
            {
                throw "\nThe card you want to take must be in the market\n";
            }

            move.takeCamel(player, market);
            break;
        }

        case(2): ///take single card
        {
            if(player.phand.size() == FULL_HAND)
            {
                throw "\nYour hand is full, you cannot take any more cards.\n";
            }

            vector<Card>::iterator it;
            string choose = player.decision();

            ///exception handling
            vector<Card>::iterator iterate;
            iterate = market.mhand.begin();
            bool found = false;
            while(iterate != market.mhand.end())
            {
                if(iterate->getType() == choose)
                {
                    found = true;
                }
                iterate++;
            }
            if(found == false)
            {
                throw "\nThe card you want to take must be in the market.\n"; //i am changed
            }

            for (it = market.mhand.begin(); it != market.mhand.end(); it++)
            {
                if (it->getType() == choose && it != market.mhand.end())
                {
                    move.fromMarket.push_back(*it);
                    move.takeSingle(player, market);
                    break;
                }
            }
            break;
        }

        case(3):    ///trading
        {
            int numCard = player.intdecision(); ///number of cards to trade

            if(numCard > 5)
            {
                throw "\nThere are only 5 cards in the market at any time, so you can only trade, at most, 5 cards.\n";
            }

            std::vector<string> check;

            if(numCard < 2)
            {        ///exception handling
                throw "\nYou must trade at least 2 cards\n";
            }

            cout << "Take cards from the market:" << endl;
            for(int i = 0; i < numCard; i++) //choose cards from market
            {
                string choose = player.decision();
                check.push_back(choose);
                if(choose == "Camel")
                {      ///exception handling
                    move.fromMarket.clear();
                    throw "\nIf you want to take camels from the market, you must take ALL the camels ONLY.\n";
                }

                ///exception handling
                vector<Card>::iterator iterate;
                iterate = market.mhand.begin();
                bool found = false;
                while(iterate != market.mhand.end())
                {
                    if(iterate->getType() == choose)
                    {
                        found = true;
                    }
                    iterate++;
                }
                if(found != true) ///exception handling
                {
                    throw "\nIf you want to trade cards from the market, the market must contain the cards you wish to trade.\n";
                }

                vector<Card>::iterator it1;
                for (it1 = market.mhand.begin(); it1 != market.mhand.end(); it1++)
                {
                    if (it1->getType() == choose && it1 != market.mhand.end())
                    {
                        move.fromMarket.push_back(*it1);
                        break;
                    }
                }
            }

            cout << "Exchange cards from your hand:" << endl;
            int tcamel = 0;
            for(int i = 0; i < numCard; i++)     //choose cards from hand do give back
            {
                string choose1 = player.decision();     ///choose
                if(std::find(check.begin(), check.end(), choose1) != check.end())   ///exception handling
                {
                    throw "\nYou cannot trade a good for the same good from the market..\n";
                }


                ///exception handling
                vector<Card>::iterator iterate;
                iterate = player.phand.begin();
                bool found = false;

                if(choose1 ==  "Camel"){
                    tcamel++;
                }

                if(tcamel > player.getHerd()){
                    throw "You do not have that many camels!";
                }

                if(choose1 != "Camel")
                {
                    while(iterate != player.phand.end())
                    {
                        if(iterate->getType() == choose1)
                        {
                            found = true;
                        }
                        iterate++;
                    }


                    if(found != true) ///exception handling
                    {
                        throw "\nIf you want to trade a from your hand, you must choose cards in you collection.\n";
                    }
                }

                vector<Card>::iterator it2 = player.herd.begin();

                if (choose1 == "Camel")
                {
                    for (it2 = player.herd.begin(); it2 != player.herd.end(); it2++)
                    {
                        if (it2->getType() == choose1 && it2 != player.herd.end())
                        { ///choose error?
                            move.fromHand.push_back(*it2);
                        }
                    }
                }
                else
                {
                    for (it2 = player.phand.begin(); it2 != player.phand.end(); it2++)
                    {
                        if (it2->getType() == choose1 && it2 != player.phand.end())
                        {
                            move.fromHand.push_back(*it2);
                        }
                    }
                }
            }
            move.takeMultiple(player, market);
            break;
        }

        case(4):    ///sell cards
        {
            if(player.phand.size() == 0)
            {           ///exception handling
                throw "\nYou have no cards to sell\n";
            }

            string goodType = player.decision(); ///type of card
            if(goodType == "Camel")
            {
                throw "\nYou can't sell camels!\n";
            }

            ///exception handling
            vector<Card>::iterator iterate;
            iterate = player.phand.begin();
            bool found = false;
            while(iterate != player.phand.end())
            {   ///exception handling
                if(iterate->getType() == goodType)
                {
                    found = true;
                }
                iterate++;
            }
            if(found == false)
            {        ///exception handling
                throw "\nThe card you want to sell must be in your hand!\n";
            }

            int numSell = 0;
            if (player.playerType() == 1)   ///human
            {
                numSell = player.intdecision(); ///number of those cards

                if(numSell == 0 || numSell < 1 || numSell > 7) ///exception handling
                {
                    throw "You can only sell between 1 and 7 cards";
                }

                if(numSell < 2 && (goodType == "Silver" || goodType == "Diamonds" || goodType == "Gold"))  ///exception handling
                {
                    throw "If you want to sell Silver, Diamonds or Gold, you must sell at least two of these at a time";
                }

                ///exception handling
                vector<Card>::iterator iterate;
                iterate = player.phand.begin();
                int found = 0;
                while(iterate != player.phand.end() && found != numSell )
                {
                    if(iterate->getType() == goodType)
                    {
                        found++;
                    }
                    iterate++;
                }
                if(found != numSell) ///exception handling
                {
                    throw "\nYou must have at least as many cards to sell as you specify.\n";
                }
            }

            else
            {
                for (unsigned int i = 0; i < player.phand.size(); i++)
                {
                    if (player.phand[i].getType() == goodType)
                    {
                        numSell++;
                    }
                }
            }
            move.sell(player, market, numSell, goodType);
            break;
        }
    }
}

void Game::printMarket(int deckSize) {
    int diamonds, gold, silver, cloth, spice, leather, b3, b4, b5, ct; //, soex;
    string c1, c2, c3, c4, c5;
    map<string, vector<int> >::iterator it;
    it = market.tgood.token.find("Diamonds");
    diamonds = it->second.size();
    it = market.tgood.token.find("Gold");
    gold = it->second.size();
    it = market.tgood.token.find("Silver");
    silver = it->second.size();
    it = market.tgood.token.find("Cloth");
    cloth = it->second.size();
    it = market.tgood.token.find("Spice");
    spice = it->second.size();
    it = market.tgood.token.find("Leather");
    leather = it->second.size();
    it = market.bonus.token.find("Three");
    b3 = it->second.size();
    it = market.bonus.token.find("Four");
    b4 = it->second.size();
    it = market.bonus.token.find("Five");
    b5 = it->second.size();
    ct = 1;
    market.printHand();
    c1 = market.mhand[0].getType2();
    c2 = market.mhand[1].getType2();
    c3 = market.mhand[2].getType2();
    c4 = market.mhand[3].getType2();
    c5 = market.mhand[4].getType2();
//    cout << market.mhand[0].getType2() << endl;
//    soex = 1;

    printf("=====================================  Market Place  ====================================\n");

//    market.tokens[0].
    //printf("Goods Tokens: Diamonds (%d)    Gold (%d)     Silver (%d)    Cloth (%d)   Spice (%d)   Leather (%d)\n", diamonds, gold, silver, cloth, spice, leather);
    //printf("Bonus Tokens: 3 (%d)      4 (%d)       5+ (%d)\n", b3, b4, b5);
    printf("Camel Token: (%d)\n", ct);
//    printf("Seals of Excellence:  \u06de (%d)\n\n", soex);

    printf("----Goods----\n"); //                     ============== Market Place ===============\n");

    printf("    ___                                                                            \n");
    printf("   /Dmd\\                                       ----Bonus----                              \n");
    printf("   \\(%d)/                                ___       ___       ___      \n", diamonds);
    printf("    ___                                / 3 \\     / 4 \\     / 5 \\                     \n");
    printf("   /Au \\                               \\(%d)/     \\(%d)/     \\(%d)/                       \n", b3, b4, b5);
    printf("   \\(%d)/                                                         \n", gold);
    printf("    ___     ______          ______     ______     ______     ______     ______     \n");
    printf("   /Ag \\   |      |        |      |   |      |   |      |   |      |   |      |   \n");
    printf("   \\(%d)/   |Deck: |        |      |   |      |   |      |   |      |   |      |  \n", silver);
    cout << "    ___    |      |        | " << c1 << " |   | " << c2 << " |   | " << c3 << " |   | " << c4 << " |   | " << c5 << " |"  << std::endl; //, deckSize, c1, c2, c3, c4, c5);
    printf("   /Clt\\   | (%.2d) |        |      |   |      |   |      |   |      |   |      |   \n", deckSize);
    printf("   \\(%d)/   |______|        |______|   |______|   |______|   |______|   |______|  \n", cloth);
    printf("    ___                                                                            \n");
    printf("   /Spc\\                                                                         \n");
    printf("   \\(%d)/                                                                         \n", spice);
    printf("    ___                                                                            \n");
    printf("   /Ltr\\                                                                          \n");
    printf("   \\(%d)/                                                                         \n", leather);
    printf("                                                                                  \n");

}


void printHand(char const * name, int number_of_cards, int herd, int soa, int rups ) {
    printf("Player %s's hand:      Rupees: %d   \u06de : %d\n", name, rups, soa);
    switch(number_of_cards){
        case 7:{
            if (herd != 0){
                printf("\t ______     ______     ______     ______     ______     ______     ______      Herd(%.2d)______\n", herd);
                printf("\t|      |   |      |   |      |   |      |   |      |   |      |   |      |            |      |\n");
                printf("\t|      |   |      |   |      |   |      |   |      |   |      |   |      |            |      |\n");
//                printf("\t| %s |   | %s   |   | %s   |   | %s   |   | %s   |   | %s   |   |%s    |            |Camels|\n",);
                printf("\t|      |   |      |   |      |   |      |   |      |   |      |   |      |            |      |\n");
                printf("\t|______|   |______|   |______|   |______|   |______|   |______|   |______|            |______|\n\n");
                break;
            }
            else{
                printf("\t ______     ______     ______     ______     ______     ______     ______    \n");
                printf("\t|      |   |      |   |      |   |      |   |      |   |      |   |      |   You have no\n");
                printf("\t|      |   |      |   |      |   |      |   |      |   |      |   |      |   camels in\n");
                printf("\t|      |   |      |   |      |   |      |   |      |   |      |   |      |   your herd!\n");
                printf("\t|      |   |      |   |      |   |      |   |      |   |      |   |      |   \n");
                printf("\t|______|   |______|   |______|   |______|   |______|   |______|   |______|   \n\n");
                break;
            }
        }

        case 6: {
            if (herd != 0){
                printf("\t ______     ______     ______     ______     ______     ______       Herd(%.2d)______\n", herd);
                printf("\t|      |   |      |   |      |   |      |   |      |   |      |             |      |\n");
                printf("\t|      |   |      |   |      |   |      |   |      |   |      |             |      |\n");
                printf("\t|      |   |      |   |      |   |      |   |      |   |      |             |      |\n");
                printf("\t|      |   |      |   |      |   |      |   |      |   |      |             |      |\n");
                printf("\t|______|   |______|   |______|   |______|   |______|   |______|             |______|\n\n");
                break;
            }

            else {
                printf("\t ______     ______     ______     ______     ______     ______    \n");
                printf("\t|      |   |      |   |      |   |      |   |      |   |      |   You have no\n");
                printf("\t|      |   |      |   |      |   |      |   |      |   |      |   camels in\n");
                printf("\t|      |   |      |   |      |   |      |   |      |   |      |   your herd!\n");
                printf("\t|      |   |      |   |      |   |      |   |      |   |      |   \n");
                printf("\t|______|   |______|   |______|   |______|   |______|   |______|   \n\n");
                break;
            }
        }

        case 5: {
            if(herd != 0) {
                printf("\t ______     ______     ______     ______     ______     Herd(%.2d) ______\n", herd);
                printf("\t|      |   |      |   |      |   |      |   |      |            |      |\n");
                printf("\t|      |   |      |   |      |   |      |   |      |            |      |\n");
                printf("\t|      |   |      |   |      |   |      |   |      |            |      |\n");
                printf("\t|      |   |      |   |      |   |      |   |      |            |      |\n");
                printf("\t|______|   |______|   |______|   |______|   |______|            |______|\n\n");
                break;
            }
            else {
                printf("\t ______     ______     ______     ______     ______   \n");
                printf("\t|      |   |      |   |      |   |      |   |      |   You have no\n");
                printf("\t|      |   |      |   |      |   |      |   |      |    camels in \n");
                printf("\t|      |   |      |   |      |   |      |   |      |     your herd!\n");
                printf("\t|      |   |      |   |      |   |      |   |      |  \n");
                printf("\t|______|   |______|   |______|   |______|   |______|  \n\n");
                break;
        }
    }

        case 4:{
            if (herd != 0){
                printf("\t ______     ______     ______     ______     Herd(%.2d) ______ \n", herd);
                printf("\t|      |   |      |   |      |   |      |            |      |\n");
                printf("\t|      |   |      |   |      |   |      |            |      |\n");
                printf("\t|      |   |      |   |      |   |      |            |      |\n");
                printf("\t|      |   |      |   |      |   |      |            |      |\n");
                printf("\t|______|   |______|   |______|   |______|            |______|\n\n");
                break;
            }
            else {
                printf("\t ______     ______     ______     ______   \n");
                printf("\t|      |   |      |   |      |   |      |   You have no\n");
                printf("\t|      |   |      |   |      |   |      |   camels in your\n");
                printf("\t|      |   |      |   |      |   |      |   herd!\n");
                printf("\t|      |   |      |   |      |   |      |   \n");
                printf("\t|______|   |______|   |______|   |______|   \n\n");
                break;
            }
        }

        case 3:{
            if(herd != 0){
                printf("\t ______     ______     ______     Herd(%.2d) ______ \n", herd);
                printf("\t|      |   |      |   |      |            |      | \n");
                printf("\t|      |   |      |   |      |            |      | \n");
                printf("\t|      |   |      |   |      |            |      | \n");
                printf("\t|      |   |      |   |      |            |      | \n");
                printf("\t|______|   |______|   |______|            |______| \n\n");
                break;
            }
            else {
                printf("\t ______     ______     ______   \n");
                printf("\t|      |   |      |   |      |  You have no\n");
                printf("\t|      |   |      |   |      |  camels in  \n");
                printf("\t|      |   |      |   |      |  your herd!\n");
                printf("\t|      |   |      |   |      |  \n");
                printf("\t|______|   |______|   |______|  \n\n");
                break;
            }
        }

        case 2: {

            if (herd != 0) {
                printf("\t ______     ______     Herd(%.2d) ______ \n", herd);
                printf("\t|      |   |      |            |      | \n");
                printf("\t|      |   |      |            |      | \n");
                printf("\t|      |   |      |            |      | \n");
                printf("\t|      |   |      |            |      | \n");
                printf("\t|______|   |______|            |______| \n\n");
                break;
            }
            else {
                printf("\t ______     ______    \n");
                printf("\t|      |   |      |   You have no\n");
                printf("\t|      |   |      |   camels in\n");
                printf("\t|      |   |      |   your herd!\n");
                printf("\t|      |   |      |   \n");
                printf("\t|______|   |______|   \n\n");
                break;
            }
        }

        case 1: {
            if(herd != 0) {
                printf("\t ______     Herd(%.2d)______    \n", herd);
                printf("\t|      |            |      |   \n");
                printf("\t|      |            |      |   \n");
                printf("\t|      |            |      |   \n");
                printf("\t|      |            |      |   \n");
                printf("\t|______|            |______|   \n\n");
                break;
            }
            else{
                printf("\t ______    \n");
                printf("\t|      |   You have no\n");
                printf("\t|      |   camels in\n");
                printf("\t|      |   your herd!\n");
                printf("\t|      |   \n");
                printf("\t|______|   \n\n");
                break;
            }
        }

        case 0: {

            if(herd != 0) {
                printf("                                Herd(%.2d) ______    \n", herd);
                printf("                                        |      |   \n");
                printf("You currently have no goods\n           |      |\n");
                printf("                                        |      |   \n");
                printf("                                        |      |   \n");
                printf("                                        |______|   \n\n");
                break;
            }

            else {
                printf("                                   \n");
                printf("                                        You have no \n");
                printf("You currently have no goods..           camels in   \n");
                printf("                                        your herd!\n");
                printf("                                        \n");
                printf("                                          \n\n");
                break;
            }
        }
        default:
            printf("\n%s somehow managed to have more than 7 \n", name);
            printf("or less than 0 cards in their hand..\n");
           // printf("\ngive them the the stuff..\n");
            //printf("--giving %s the stuff..\n", name);
    }
}

void Game::strut() {
    printf(" _______    \n");
    printf("|#Strut |   \n");
    printf("| \\('-')|  \n");
    printf("|  (  (>|   \n");
    printf("|  /   \\|  \n");
    printf("|_______|   \n\n");
}
