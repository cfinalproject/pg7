#ifndef GAME_H_INCLUDED
#define GAME_H_INCLUDED
#include <string>
#include <vector>
#include "Player.h"
#include "Market.h"
#include "Move.h"

class Game
{
    //friend class Deck;
    std::vector<Player*> players;//Player[] players;
    Market market;
    Move move;
public:
    Game();
    Game(std::string);
    Game(std::string, std::string);
    void playround();
    void strut();
    void deal();
    void makemove(Player&);
    void printMarket(int);
};

#endif // GAME_H_INCLUDED
