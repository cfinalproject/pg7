#ifndef MOVE_H_INCLUDED
#define MOVE_H_INCLUDED
#include <vector>
#include "Card.h"
#include "Token.h"
#include "Player.h"
#include "Market.h"
//#include "Human.h"

class Move
{
    friend class Game;
    friend class Market;
    friend class Player;
public:
//public:
//    Move();
    //~Move();

    void sell(Player&, Market& market, int, std::string);
    void takeBonus(int, Player&, Market&);
    void takeTokens(int, std::string, Player&, Market&); //and int?
    Card take_card(std::vector<Card> &, Card &);
    Card take_card(std::vector<Card> &);
    void place_card(std::vector<Card> & , Card &);
    void takeSingle(Player &, Market &);
    void takeMultiple(Player &, Market &);
    void takeCamel(Player &, Market &);
private:
    std::vector<Card> fromHand;
    std::vector<Card> fromMarket;
};
#endif // MOVE_H_INCLUDED
