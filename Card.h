#ifndef CARD_H_INCLUDED
#define CARD_H_INCLUDED
#include <string>
//#include "Market.h"
//#include "Deck.h"
//#include "Player.h"
/*
 Description: Card is an abstract base class.
 Concrete derived classes include Cgood and Ccamel.
 A Card represents the common aspects of all cards,
 including a type.
*/
class Card
{
    //friend class Game;
    friend class Market;
//    friend class Player;
    friend class Deck;

public:
    std::string getType() const{ return type; };
    int getRank() const{ return rank; };
    std::string getType2() const;
    Card(std::string, int);
    Card();
//    ~Card();
    Card(const Card &);
    bool operator==(Card b);
private:
    std::string type;
    int rank;
};
#endif // CARD_H_INCLUDED
