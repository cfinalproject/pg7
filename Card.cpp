#include <string>
#include <iostream>
#include "Card.h"
//#include "Deck.h"

using std::string;
using std::cout;
using std::endl;

Card::Card()
{

}
Card::Card(string t, int r) {
    type = t;
    rank = r;
}

//a copy constructor
Card::Card(const Card & card) {
    this->type = card.type;
    this->rank = card.rank;
}

//a destructor
//Card::~Card() {
//    std::cout<<"A "<<type<<" card is being destroyed"<<std::endl;
//}

//overloaded equality operator
bool Card::operator==(Card b) {
    return (this->type == b.type);
}

string Card::getType2() const{
    if (type == "Diamonds") {
	return "Dmnd";
    }
    else if (type == "Gold") {
	return "Gold";
    }
    else if (type == "Silver") {
	return "Slvr";
    }
    else if (type == "Cloth") {
	return "Clth";
    }
    else if (type == "Spice") {
	return "Spc ";
    }
    else if (type == "Leather") {
	return "Lthr";
    }
    else if (type == "Camel") {
	return "Cml ";
    }
    return type;
}
