#ifndef PLAYER_H_INCLUDED
#define PLAYER_H_INCLUDED

#include <vector>
#include <string>
#include <map>
#include "Card.h"
//#include "Market.h"
#include "Token.h"
//#include "Game.h"
//#include "Move.h" //not included before

/*
 Description: Player is an object
 with a vector of Cards, a vector
 of Camel Cards, and an array of
 Tokens. The Player can interact
 with the Market and other Players.
*/

class Player
{
    friend class Game;
    friend class Move;
    friend class Market;
//    string name;

public:

//public:
    Player();
    virtual ~Player();
//    ~Player();
    std::vector<Card> phand;
    std::string getName() const { return name; };
    void addcard(const Card&);
    void remove_card(Card&);
    void printHand();
    //int getHerd(std::vector<Card> &h) const { return h.size(); };
    int getHerd() const { return herd.size(); };
    void takeBonus(int);
    void takeTokens(int, std::string);
    virtual int makemove() = 0; //pure virtual function makes this an abstract base class
    virtual std::string decision() = 0; ///type of card
    virtual int intdecision() = 0; ///number of cards to trade
    int playerType() const { return type; };
    std::vector<std::string> market;
    //int get_score() const {return this->points};


private:
    int seal;
    bool isWinner;
    std::map<std::string, int> tokens;
    //std::vector<Card> phand;
    std::vector<Card> herd;
    std::string name;
    int points;
protected:
    int type;

};

#endif // PLAYER_H_INCLUDED
