#include <iostream>
#include <string>
#include <map>
#include <vector>
//#include "Token.h"
#include "Tgood.h"

using std::vector;
using std::map;
using std::string;
using std::ostream;
using std::endl;

Tgood::Tgood()
{
    int i, j, k, l;
    k = 0;
    string type[] = {"Diamonds", "Gold", "Silver", "Cloth", "Spice", "Leather"};
    int num[] = {2, 2, 1, 4, 4, 4};
    int val[17][2] = {
			{7, 2},
			{5, 3}, {6, 2}, {5, 3}, {5, 5}, {5, 1}, {3, 2}, {2, 2}, {1, 2}, {5, 1}, {3, 2}, {2, 2}, {1, 2}, {4, 1}, {3, 1}, {2, 1}, {1, 6}
    };
    for (i = 0; i < 6; i++) {
	vector<int> val_tokens;
        for (j = 0; j < num[i]; j++) {
	    for (l = 0; l < val[k][1]; l++) {
                val_tokens.push_back(val[k][0]);
	    }
	    k ++;
	}
	token.insert(make_pair(type[i], val_tokens));
    }
}

ostream& Tgood::print(ostream& os) const {
//    int i;
//    for (i = 0; i < 6; i++) {
        for (map<string, vector<int> >::const_iterator it = token.begin(); it != token.end(); it++) {
            os << it->first << ":	";
	    for (vector<int>::const_iterator it2 = it->second.begin(); it2 != it->second.end(); it2++) {
		os << *it2 << "	";
	    }
	    os << endl;
	}
//    }
    return os;
}
