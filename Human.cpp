#include <iostream>
#include <vector>
#include <string>
#include "Human.h"
#include "Player.h"

using std::cout;
using std::cin;
using std::endl;
using std::string;

Human::Human()
{
    type = 1;
}
/*Human::~Human()
{

}*/

int Human::makemove(){
    int choice;

    cout << "What move would you like to make?" << endl;
    cout << "1) Take card(s)" << endl;
    cout << "2) Trade cards" << endl;
    cout << "3) Sell cards" << endl;
    cin >> choice;

    if(choice == 1)
    {
        int choice2;
        cout << "Choose option" << endl;
        cout << "1) Take all camels" << endl;
        cout << "2) Take single card" << endl;
        cin >> choice2;

        if(choice2 == 1)
            return 1;
        if(choice2 == 2)
            return 2;
    }

    if(choice == 2) ///trade cards
        return 3;
    if(choice == 3) ///sell cards
        return 4;

    return 1;
}

string Human::decision(){
    retry:
    try{
    string type;
    cout << "What type of card?" << endl;
    cin >> type;
    if(type != "Spice" && type != "Leather" && type != "Cloth" && type != "Diamonds" && type != "Silver" && type != "Gold" && type != "Camel") {
        throw "Type must be 'Camel', Spice', 'Cloth', 'Leather', 'Diamonds', 'Silver', or 'Gold'. Check spelling (and capitalization) and try again.";
    }
    return type;
    }
    catch(char const* mes) {
        cout<<mes<<endl;
        goto retry;
    }
}

int Human::intdecision(){
    //try{
    int num;
    cout << "How many cards?" << endl;
    cin >> num;
    return num;

}
