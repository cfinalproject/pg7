#ifndef TGOOD_H_INCLUDED
#define TGOOD_H_INCLUDED
#include <string>
#include <map>
#include <vector>
#include "Token.h"

/*
 Description: Tgood is an inherited class
 of Token. It contains a map of strings
 and vectors. The string is the type of
 Good while the vector of ints describe
 the value.
*/
class Tgood: public Token
{
//public:
    friend class Move;
    friend class Market;
    friend class Game;
    //map<string, vector<int> > tgood;

public:
    //map<string, vector<int> > tgood;
    Tgood();
//    ~Tgood();
    std::ostream& print(std::ostream&) const;
};
#endif // TGOOD_H_INCLUDED
