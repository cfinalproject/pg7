#ifndef BONUS_H_INCLUDED
#define BONUS_H_INCLUDED
#include <map>
#include <vector>
#include <string>
#include "Token.h"

/*
 Description: Bonus is an inherited class
 of Token. It contains a map of strings
 and vectors. The string is the type of
 Bonus while the vector of ints describe
 the value.
*/
class Bonus: public Token
{
    friend class Move;
    friend class Market;
    friend class Game;

public:
    //map<string, vector<int> > btoken;
    Bonus();
//    Bonus~();
    void shuffle();
    std::ostream& print(std::ostream&) const;
};

#endif // BONUS_H_INCLUDED
