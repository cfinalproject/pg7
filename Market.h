#ifndef MARKET_H_INCLUDED
#define MARKET_H_INCLUDED

#include <stdio.h>
#include <vector>
#include "Deck.h"
#include "Card.h"
#include "Token.h"
#include "Bonus.h"
#include "Tgood.h"
//#include "Move.h"
#include "Player.h"

/*
 Description: Market is an object that is
 the area where the gameplay happens. It
 has all of the elements necessary for gameplay
 that Player does not.
*/
class Market
{
    friend class Game;
    friend class Move;
    friend class Player;
    friend class AI;
public:
        //constructor
	Market();
//   void make_move();
    void add_card(const Card&);
    void remove_card(Card&);
    void deal();
	void printHand();
//        Token remove_token(std::string);
        //Card remove_token(std::string type);

private:
	std::vector<Player*> player;
    Deck deck;
    //Move move;
    std::vector<Card> mhand;
//  std::vector<Token> tokens;
	Bonus bonus;
	Tgood tgood;
	//Token( ("Camel"), 5) tcamel;
	Token tcamel;
    std::vector<Card> discard;
};
#endif // MARKET_H_INCLUDED
