#include <iostream>
#include <string.h>
#include <map>
#include <time.h>
#include "Player.h"
#include "Market.h"
#include "Move.h"
#include <algorithm>

#define FULL_HAND 7

using std::vector;
using std::map;
using std::string;
using std::cout;
using std::endl;

Card Move::take_card(std::vector<Card> & collection, Card & card) {
    Card temp_card = card;
    std::vector<Card>::iterator pos = std::find(collection.begin(), collection.end(), card);
    collection.erase(pos);
    return temp_card;
}

//remove the specified card from the marketplace and place it in the specified players hand
void Move::takeSingle(Player& player, Market& market) {
    market.remove_card(fromMarket[0]);
    player.addcard(fromMarket[0]);
    Card newCard(market.deck.cards[0]);
    market.deck.cards.erase(market.deck.cards.begin());
    market.add_card(newCard);
    fromMarket.clear();
}

//remove the specified number of the specified cards from the marketplace and put them in the specified players hand
void Move::takeMultiple(Player& player, Market& market) {

    for (unsigned int i = 0; i < fromMarket.size(); i++) {
    //+cout << "FROMHAND SIZE: " << fromHand.size() << endl;
	player.addcard(fromMarket[i]);
	market.add_card(fromHand[i]);

	market.remove_card(fromMarket[i]);

	player.remove_card(fromHand[i]);

    }
    fromMarket.clear();
    fromHand.clear();
}


//takes all the camels from the market and places them in the players herd
void Move::takeCamel(Player& player, Market& market) {
    unsigned int i = 0;
    int j = 0;


    while(i < 5 && j < 5) { ///j = index, i = # loops
        if (market.mhand[j].getType() == "Camel") {
            Card camel = market.mhand[j];
            fromMarket.push_back(camel);
            market.remove_card(market.mhand[j]);
            Card newCard = market.deck.cards[0];
            market.deck.cards.erase(market.deck.cards.begin());
            market.add_card(newCard);
            j--;
        }
        j++;
        i++;
    }
    //cout << "fromMarket size: " << fromMarket.size() << endl;
    for (i = 0; i < fromMarket.size(); i++) {
        player.addcard(fromMarket[i]);
    }
    fromMarket.clear();
}

void Move::sell(Player& player, Market& market, int numSell, string goodType)
{
    int j = 0;
    while (j < numSell) {
        for(unsigned int i = 0; i < player.phand.size(); i++) {
            if (player.phand[i].getType() == goodType) {
            Card sell = player.phand[i];
            player.remove_card(sell);
            market.discard.push_back(player.phand[i]);
            j++;
            break;
            }
        }
    }
    takeTokens(numSell, goodType, player, market);
    if (numSell >= 3) {
        takeBonus(numSell, player, market);
    }
}

void Move::takeBonus(int numSell, Player& player, Market& market)
{
    int i, r, size;
    if(numSell == 3){
        std::map<string, vector<int> >::iterator it = market.bonus.token.find("Three");
        srand(time(NULL));
        size = it->second.size();
        r = rand() % size;
        vector<int>::iterator it2 = it->second.begin();
        for (i = 0; i < r; i++) {
            it2++;
        }
        if(it->second.size() != 0){
            player.tokens.insert(make_pair(it->first, *it2));
            player.points += *it2;
            it->second.erase(it2);
        }
        //Player gets bonus token 3
    }
    else if(numSell == 4)
    {
	std::map<string, vector<int> >::iterator it = market.bonus.token.find("Four");
        srand(time(NULL));
        size = it->second.size();
        r = rand() % size;
        vector<int>::iterator it2 = it->second.begin();
        for (i = 0; i < r; i++) {
             it2++;
        }
        if(it->second.size() != 0){
            player.tokens.insert(make_pair(it->first, *it2));
            player.points += *it2;
            it->second.erase(it2);
        }
        //Player gets bonus token 4
    }
    else if(numSell >= 5)
    {
	std::map<string, vector<int> >::iterator it = market.bonus.token.find("Five");
        srand(time(NULL));
        size = it->second.size();
        r = rand() % size;
        vector<int>::iterator it2 = it->second.begin();
        for (i = 0; i < r; i++) {
             it2++;
        }
        if(it->second.size() != 0){
            player.tokens.insert(make_pair(it->first, *it2));
            player.points += *it2;
            it->second.erase(it2);
        }
        //Player gets bonus token 5
    }
}

void Move::takeTokens(int numTake, string type, Player& player, Market& market) {
	for (int i = 0; i < numTake; i++) {
   	    std::map<string, vector<int> >::iterator it = market.tgood.token.find(type);
	    vector<int>::iterator it2 = it->second.begin();
        if(it->second.size() != 0){
            player.points += *it2;
            it->second.erase(it2);
        }
	}
}

