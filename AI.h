#ifndef AI_H_INCLUDED
#define AI_H_INCLUDED
#include <vector>
#include <string>
#include "Player.h"
#include "Move.h"

//using namespace std;

class AI: public Player
{
    friend class Market;
    //Move move = *new Move();
private:
    int moh;
    //std::vector<std::string> market;

public:
    AI();
    //std::vector<std::string> market;
    int makemove();
    std::string decision();
    int intdecision(); ///number of cards to trade
    int playerType() const { return type; };
};

#endif // AI_H_INCLUDED
