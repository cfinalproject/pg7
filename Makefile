CC = g++
CFLAGS= -std=c++11 -pedantic -Wall -Wextra -O -g

JaipurMain: Token.cpp Token.h Tgood.cpp Tgood.h Bonus.cpp Bonus.h Card.cpp Card.h Deck.cpp Deck.h Player.cpp Player.h Human.cpp Human.h AI.cpp AI.h Market.cpp Market.h  Move.cpp Move.h Game.cpp Game.h JaipurMain.cpp
	$(CC) $(CFLAGS) -o JaipurMain Token.cpp Tgood.cpp Bonus.cpp Card.cpp Deck.cpp Player.cpp Human.cpp AI.cpp Market.cpp Move.cpp Game.cpp JaipurMain.cpp

clean:
	\rm -rf *.o JaipurMain
