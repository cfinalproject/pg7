#ifndef HUMAN_H_INCLUDED
#define HUMAN_H_INCLUDED
#include "Player.h"
#include "Move.h" //compiling version doesn't have this, not sure if it makes a difference
#include <string>

class Human: public Player
{
    //Move move = *new Move();
private:
//std::vector<std::string> market;

public:
    Human();
    //Human(string);
    //~Human();
    //string getName(){ return name; };
    //std::vector<std::string> market;
    int makemove();
    std::string decision();
    int intdecision(); ///number of cards to trade
    int playerType() const { return type; };
};


#endif // HUMAN_H_INCLUDED
