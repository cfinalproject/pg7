#ifndef DECK_H_INCLUDED
#define DECK_H_INCLUDED
#include <vector>
#include "Card.h"

/*
 Description: Deck contains all of
 the cards in the game. It contains
 functions for shuffling the deck and
 distributing cards to the players
 and the market.
*/

class Deck
{
    friend class Market;
    friend class Player;
    friend class Game;
//    vector<Card> cards;

public:
    Deck();
    std::vector<Card> cards;
    //void check(Card);
    void shuffle();
//    void deal();
};
#endif // DECK_H_INCLUDED
