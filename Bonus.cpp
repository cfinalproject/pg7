#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <time.h>
#include <stdlib.h>
#include "Token.h"
#include "Bonus.h"

using std::string;
using std::vector;
using std::map;
using std::ostream;
using std::endl;

Bonus::Bonus(){
    int i, j, k, l;
    k = 0;
    string type[] = {"Three", "Four", "Five"};
//    int num[] = {3, 3, 3};
    int val[9][2] = {
		     {3, 2}, {2, 3}, {1, 2}, {6, 2}, {5, 2}, {4, 2}, {10, 2}, {9, 1}, {8, 2}
    };
    for (i = 0; i < 3; i++) {
        vector<int> val_bonus;
        for (j = 0; j < 3; j++) {
            for (l = 0; l < val[k][1]; l++) {
                    val_bonus.push_back(val[k][0]);
            }
            k++;
        }
        token.insert(make_pair(type[i], val_bonus));
    }
}

/*Bonus::~Bonus()
{

}*/

void Bonus::shuffle(){
    int i, j, num, temp_val;
    i = 0;
    int nums[] = {7, 6, 5};

    srand(time(NULL));
    for (j = 0; j < 3; j++) {
        for (map<string, vector<int> >::iterator it = token.begin(); it != token.end(); ++it) {
            num = rand() % nums[i];
            for (vector<int>::iterator it2 = it->second.begin(); it2 != it->second.end(); it2++) {
                temp_val = it2[i];
                it2[i] = it2[num];
                it2[num] = temp_val;
            }
        }
    }
}

ostream& Bonus::print(ostream& os) const {
    for (map<string, vector<int> >::const_iterator it = token.begin(); it != token.end(); it++) {
        os << it->first << ":	";
	for (vector<int>::const_iterator it2 = it->second.begin(); it2 != it->second.end(); it2++) {
	    os << *it2 << "	";
	}
	os << endl;
    }
    return os;
}
